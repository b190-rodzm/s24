// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
let num = 2;
let getCube = 2 ** 3;

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
console.log(`The cube of ${num} is ${getCube}`);

// 5. Create a variable address with a value of an array containing details of an address.

let address = ["258 Washington Ave NW", "California", 90011,];

// 6. Destructure the array and print out a message with the full address using Template Literals.

let [street, state, zipCode] = address;
console.log(`I live at ${street},${state} ${zipCode}`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.

let animal = {
  animalName: "Lolong",
  kind: "saltwater crocodile",
  weight: 1075,
  height: [20, 3]
};

// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

let { animalName, kind, weight, height} = animal;
console.log(`${animalName} was a ${kind}. He weighed at ${weight} kgs with a measurement of ${height[0]} ft ${height[1]}.`);

// 9. Create an array of numbers.

let numbers = [1,2,3,4,5];

// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

numbers.forEach((numbers) => console.log(numbers));

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

const reduceNumber = numbers.reduce((x,y)=> x + y, 0);
console.log(reduceNumber);

// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

class Dog{
  constructor (name, age, breed){
    this.name = name;
    this.age = age;
    this.breed =  breed;
  }
};

// 13. Create/instantiate a new object from the class Dog and console log the object.
let myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
console.log(myDog);

// 14. Create a git repository named S24.
// 15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 16. Add the link in Boodle.